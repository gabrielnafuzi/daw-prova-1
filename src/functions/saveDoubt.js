const fs = require('fs')
const path = require('path')
const doubts = require('../data/doubts.json')

const saveDoubt = ({ email, description }) => {
  const [lastDoubt = {}] = doubts.reverse()

  const doubtId = (lastDoubt.id || 0) + 1

  fs.writeFile(
    path.resolve(__dirname, '..', 'data/doubts.json'),
    JSON.stringify(
      [
        ...doubts,
        { id: doubtId, email, description, createdAt: new Date().getTime() }
      ].sort((a, b) => a.id - b.id),
      null,
      2
    ),
    err => {
      if (err) throw new Error(err.message)
    }
  )
}

module.exports = saveDoubt
