const mobileButton = document.querySelector('.mobile__button')
const headerNav = document.querySelector('.header__nav')

export const handleActiveMobileClass = method => {
  mobileButton.classList[method]('active')
  headerNav.classList[method]('active')
}

mobileButton.addEventListener('click', e => {
  e.stopPropagation()
  handleActiveMobileClass('toggle')
})

window.addEventListener('click', () => handleActiveMobileClass('remove'))
