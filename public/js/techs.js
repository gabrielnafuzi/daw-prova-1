export default [
  {
    name: 'JavaScript',
    color: '#f7df1d'
  },
  {
    name: 'TypeScript',
    color: '#377bcc'
  },
  {
    name: 'Vue.js',
    color: '#4eb883'
  },
  {
    name: 'Nuxt.js',
    color: '#567c84'
  },
  {
    name: 'Vuetify',
    color: '#aedeff'
  },
  {
    name: 'Sass',
    color: '#cf649a'
  },
  {
    name: 'GraphQL',
    color: '#e535ab'
  },
  {
    name: 'Apollo',
    color: '#000'
  },
  {
    name: 'Firebase',
    color: '#f6c93f'
  },
  {
    name: 'Jest',
    color: '#c63d14'
  },
  {
    name: 'VSCode',
    color: '#3c89ca'
  },
  {
    name: 'Figma',
    color: '#a259ff'
  },
  {
    name: 'Git',
    color: '#e46d5b'
  },
  {
    name: 'GitHub',
    color: '#161614'
  },
  {
    name: 'Bitbucket',
    color: '#2684ff'
  },
  {
    name: 'Node.js',
    color: '#8CC84B'
  },
  {
    name: 'Yarn',
    color: '#2693bf'
  },
  {
    name: 'npm',
    color: '#cb3837'
  }
]
