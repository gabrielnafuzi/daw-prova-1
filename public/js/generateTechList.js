import techs from './techs.js'
import changePrimaryColor from './changePrimaryColor.js'

const techsContainer = document.querySelector('.👨‍💻')

const makeTechTemplate = ({ name, color }) => {
  const techTemplate = document.createElement('div')

  techTemplate.addEventListener('mouseover', () => changePrimaryColor(color))
  techTemplate.setAttribute('title', name)
  techTemplate.classList.add('🧽')
  techTemplate.innerHTML = `
    <img src="/assets/logos/${name}.svg" alt="${name}" />
    <span>${name}</span>`

  return techTemplate
}

const addTechsToContainer = () => {
  techs.forEach(tech => {
    techsContainer.appendChild(makeTechTemplate(tech))
  })
}

addTechsToContainer()
