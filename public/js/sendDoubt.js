import useToast from './useToast.js'

const doubtForm = document.querySelector('[data-js="doubt-form"]')
const emailInput = document.querySelector('#email')
const descriptionInput = document.querySelector('#description')
const sendDoubtButton = document.querySelector('.🍌')

const handleSubmit = event => {
  event.preventDefault()

  sendDoubt(emailInput.value, descriptionInput.value)
}

const sendDoubt = async (email, description) => {
  sendDoubtButton.classList.add('loading')
  sendDoubtButton.setAttribute('disabled', true)

  try {
    const response = await fetch('/api/duvida', {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify({ email, description }),
      method: 'POST'
    })

    const jsonResponse = await response.json()

    if (!response.ok) throw new Error(jsonResponse.error)

    useToast({ message: jsonResponse.message, color: 'success' })
    resetForm()
  } catch (e) {
    console.error('Algo deu errado: ', e.message)

    useToast({ message: e.message, color: 'error' })
  } finally {
    sendDoubtButton.classList.remove('loading')
    sendDoubtButton.removeAttribute('disabled')
  }
}

const resetForm = () => {
  emailInput.value = ''
  descriptionInput.value = ''
}

doubtForm.addEventListener('submit', handleSubmit)
