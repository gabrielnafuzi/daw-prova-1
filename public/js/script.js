import './menuMobile.js'
import changePrimaryColor from './changePrimaryColor.js'

const primaryColor = localStorage.getItem('primary-color') || '#4eb883'

changePrimaryColor(primaryColor)
