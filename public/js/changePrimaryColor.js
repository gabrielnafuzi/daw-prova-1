import HEXLightenDarkenColor from './HEXLightenDarkenColor.js'

export default color => {
  const darkenColor = HEXLightenDarkenColor(color, -16)

  document.documentElement.style.setProperty('--primary', color)
  document.documentElement.style.setProperty('--primary-darken', darkenColor)
  localStorage.setItem('primary-color', color)
}
