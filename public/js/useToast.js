export default function useToast({ message, color, time = 4000 }) {
  const toastDivExist = document.querySelector('#toast-div')

  if (toastDivExist) toastDivExist.remove()

  const toast = createToastDiv(color, message)

  document.body.appendChild(toast)
  animateToast('enter', toast)

  setTimeout(() => removeToast(toast), time)
}

const createToastDiv = (color, message) => {
  const selectedColor = colors[color] || colors.default
  const toastDiv = document.createElement('div')

  toastDiv.id = 'toast-div'
  toastDiv.style = `${baseStyle} background: ${selectedColor}`
  toastDiv.innerHTML = `
    <span style="font-size: 1.665rem; font-weight: bold; color: #fff">
      ${message}
    </span>`

  return toastDiv
}

const removeToast = async toast => {
  await animateToast('exit', toast).finished

  toast.remove()
}

const animateToast = (state, toast) => {
  const stateReverse = state === 'enter' ? 'exit' : 'enter'
  const baseState = {
    enter: { opacity: 1, bottom: '2.5rem' },
    exit: { opacity: 0, bottom: '-4rem' }
  }

  const animation = { ...baseState[stateReverse], ...baseState[state] }

  return toast.animate(animation, {
    duration: 500,
    fill: 'forwards',
    easing: 'ease'
  })
}

const colors = {
  default: '#656565',
  success: '#4cb050',
  error: '#e62a23',
  warning: '#f19703'
}

const baseStyle = `
  display: flex;
  justify-content: center;
  align-items: center;

  position: fixed;
  bottom: -4rem;
  right: 3rem;

  max-width: max-content;
  height: 4rem;
  z-index: 9999;

  border-radius: 0.4rem;
  padding: 1.2rem;
`
