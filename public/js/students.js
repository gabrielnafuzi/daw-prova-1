import debounce from './debounce.js'
const queryInput = document.querySelector('#query')
const studentsTable = document.querySelector('[data-js="students-table"]')
const warningMessageSpan = document.querySelector('[data-js="warning-message"]')
const studentsLength = document.querySelector('[data-js="students-length"]')

const handleSubmit = async e => {
  e.preventDefault()
  warningMessageSpan.textContent = ''

  const query = queryInput.value.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '')

  try {
    const response = await fetch(`/api/alunos?name=${query}`)
    const studentsJson = await response.json()

    createStudentsList(studentsJson)
  } catch (error) {
    console.error(error)

    warningMessageSpan.setAttribute('data-error', 'error')
    warningMessageSpan.textContent =
      'Algo deu errado na busca, tente novamente!'
  }
}

const createStudentsList = studentsList => {
  const template = studentsList.reduce(
    (acc, cur) => acc + createStudentTemplate(cur),
    ''
  )

  if (!template) {
    warningMessageSpan.textContent = 'Nenhum aluno(a) foi encontrado(a). 🤖'
  }

  studentsLength.textContent = `Quantidade de alunos(as) encontrados(as): ${studentsList.length}`
  studentsTable.innerHTML = template
}

const createStudentTemplate = student =>
  `<tr>
    <td>${student.nome}</td>
    <td>${student.email}</td>
  </tr>
  `

queryInput.addEventListener('input', debounce(handleSubmit, 300))
window.addEventListener('load', handleSubmit)
