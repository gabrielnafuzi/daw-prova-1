const { Router } = require('express')
const routes = Router()
const path = require('path')

const saveDoubt = require('./functions/saveDoubt')
const doubts = require('./data/doubts.json')
const students = require('./data/alunos.json')
const dir = path.resolve(__dirname, '..')

const sleep = require('./utils/sleep')

routes.get('/', (req, res) => {
  res.sendFile(`${dir}/views/index.html`)
})

routes.get('/alunos', (req, res) => {
  res.sendFile(`${dir}/views/alunos.html`)
})

routes.get('/eu', (req, res) => {
  res.sendFile(`${dir}/views/eu.html`)
})

routes.get('/duvida', (req, res) => {
  res.sendFile(`${dir}/views/duvida.html`)
})

routes.get('/duvidas', (req, res) => {
  res.sendFile(`${dir}/views/duvidas.html`)
})

routes.get('/api/alunos', (req, res) => {
  const query = req.query.name.toLowerCase()

  const filteredStudents = students.filter(student =>
    student.nome.toLowerCase().includes(query)
  )

  res.status(200).json(filteredStudents)
})

routes.get('/api/duvidas', (req, res) => {
  res.status(200).json(doubts)
})

routes.post('/api/duvida', async (req, res) => {
  await sleep(1500)
  const { email, description } = req.body

  if (!email) {
    return res.status(400).send({ error: 'Campo email não pode ser vazio!' })
  }

  if (!description) {
    return res
      .status(400)
      .send({ error: 'Campo descrição não pode ser vazio!' })
  }

  try {
    saveDoubt({ email, description })

    res.status(200).json({ message: 'Dúvida enviada com sucesso!' })
  } catch (e) {
    console.error(e)
    res.status(500).send({ error: 'Falha ao enviar a dúvida!' })
  }
})

module.exports = routes
