const express = require('express')
const routes = require('./routes')
const path = require('path')

const app = express()

const dir = path.resolve(__dirname, '..')
const PORT = 3000

app.use(express.static(`${dir}/public`))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(routes)

app.listen(PORT, () => {
  console.log(`🚀 Servidor está rodando em: http://localhost:${PORT}`)
})
