import { handleActiveMobileClass } from './menuMobile.js'
const doubtsSection = document.querySelector('.📖')
const orderDoubtsContainer = document.querySelector('.📏')
const orderSelect = document.querySelector('[data-js="doubt-order-button"]')
const orderText = document.querySelector('[data-js="doubt-order-text"]')
const orderListItems = document.querySelector(
  '[data-js="doubt-order-list-items"]'
)
const maxDescriptionLength = 100
let doubts = null

const listItemClickEvent = item => {
  const listValue = item.getAttribute('data-list-value')

  orderText.textContent = item.innerText
  handleOrderSelect(listValue)
}

const setClickEventToDoubtListItem = () => {
  const listItemsArray = [...orderListItems.children]

  listItemsArray.forEach(item =>
    item.addEventListener('click', () => listItemClickEvent(item))
  )
}

orderSelect.addEventListener('click', e => {
  e.stopPropagation()
  handleActiveMobileClass('remove')
  handleActiveorderDoubtsContainer('toggle')
})

const handleActiveorderDoubtsContainer = method => {
  orderDoubtsContainer.classList[method]('👀')
}

const handleOrderSelect = orderBy => {
  const sortedDoubts = sortDoubts(orderBy)
  addDoubtsToList(sortedDoubts)
}

const sortDoubts = orderBy => {
  const ordersTypes = {
    older: (a, b) => a.createdAt - b.createdAt,
    newer: (a, b) => b.createdAt - a.createdAt
  }

  return doubts.sort(ordersTypes[orderBy])
}

const baseDoubtTemplate = `
<div id="DOUBT_ID" class="❓">
  <div class="💁">
    <span>DOUBT_EMAIL</span>
    <span class="⏲">DOUBT_DATE</span>
  </div>

  <div class="🤷">
    <p id="description-DOUBT_ID">DOUBT_DESCRIPTION</p>
  </div>
</div>
`

const addDoubtsToList = doubtsList => {
  const fullTemplate = doubtsList.reduce(generateDoubtTemplate, '')

  doubtsSection.innerHTML = fullTemplate
  addEventToSeeMoreButtons()
}

const generateDoubtTemplate = (acc, cur) => acc + createTemplate(cur)

const createTemplate = doubt => {
  const formatedDate = new Date(doubt.createdAt).toLocaleDateString('pt-BR')
  let description = doubt.description

  if (description.length > maxDescriptionLength) {
    const descWithHundredChar = description.substring(0, maxDescriptionLength)

    description = `${descWithHundredChar} ... <strong class="👁">Ver mais</strong>`
  }

  return baseDoubtTemplate
    .replace(/DOUBT_ID/g, doubt.id)
    .replace(/DOUBT_EMAIL/g, doubt.email)
    .replace(/DOUBT_DATE/g, formatedDate)
    .replace(/DOUBT_DESCRIPTION/g, description)
}

const addEventToSeeMoreButtons = () => {
  const seeMoreButtons = document.querySelectorAll('.👁')

  seeMoreButtons.forEach(btn => btn.addEventListener('click', seeMoreEvent))
}

const seeMoreEvent = event => {
  const seeMoreBtn = event.target
  const doubtParagraph = seeMoreBtn.parentElement
  const doubtId = doubtParagraph.id.replace('description-', '')

  const fullDescription = getFullDescription(doubtId)

  doubtParagraph.innerHTML = `${fullDescription} <strong class="🙈">Ver menos</strong>`
  addSeeLessEvent(doubtParagraph)
}

const getFullDescription = id => {
  const foundDoubt = doubts.find(doubt => String(doubt.id) === id)

  return foundDoubt.description
}

const addSeeLessEvent = paragraphEl => {
  const seeLessBtn = paragraphEl.querySelector('.🙈')

  seeLessBtn.addEventListener('click', seeLessEvent)
}

const seeLessEvent = event => {
  const seeLessEvent = event.target
  const doubtParagraph = seeLessEvent.parentElement

  const descWithHundredChar = doubtParagraph.textContent.substring(
    0,
    maxDescriptionLength
  )

  doubtParagraph.innerHTML = `${descWithHundredChar} ... <strong class="👁">Ver mais</strong>`
  addEventToSeeMoreButtons()
}

const getDoubts = async () => {
  const response = await fetch('/api/duvidas')
  doubts = await response.json()

  if (!doubts.length) {
    orderDoubtsContainer.classList.add('hide')
    orderDoubtsContainer.previousElementSibling.classList.add('hide')

    doubtsSection.innerHTML =
      '<span class="🧐">Nenhuma dúvida foi feita ainda. 🧐</span>'

    return
  }

  const sortedDoubts = sortDoubts('newer')
  addDoubtsToList(sortedDoubts)
}

getDoubts()
orderText.textContent = 'Mais novas'
setClickEventToDoubtListItem()

window.addEventListener('click', () => {
  handleActiveorderDoubtsContainer('remove')
})
