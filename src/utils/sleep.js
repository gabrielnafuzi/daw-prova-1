/**
 * @description função feita para simular algum tempo de resposta de uma API
 */
const sleep = (timeout = 1500) => {
  return new Promise(resolve => setTimeout(resolve, timeout))
}

module.exports = sleep
